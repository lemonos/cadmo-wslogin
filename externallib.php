<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External Web Service Login
 *
 * @package    localwslogin
 * @copyright  2012 Cadmo Conocimiento (http://www.cadmoweb.com/)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 
define('WSLOGIN_NO_LOGIN_ERRROR', 0);
define('WSLOGIN_WRONG_PASSWORD', 1); 
define('WSLOGIN_USER_NO_EXISTS', 2);
define('WSLOGIN_NO_ACCESS_TO_COURSE', 3);
define('WSLOGIN_COURSE_NO_EXISTS', 4);
 
 
require_once($CFG->libdir . "/externallib.php");

class local_wslogin_external extends external_api {

    /**
     * Returns description of method  get_email_login parameters
     * @return external_function_parameters
     */
    public static function get_email_login_parameters() {
        return new external_function_parameters(
                array( 
                        'email' => new external_value(PARAM_EMAIL, 'Dirección de correo electrónico a utilizar para validar contra el repositorio de usuarios de moodle'),
                        'password' => new external_value(PARAM_TEXT, 'Contraseña a usar para la autenticación en el proceso de validación'),
                        'courseid' => new external_value(PARAM_INT, 'Identificador del curso al que se pretende acceder. Tiene que ser un identificador de curso valido.', VALUE_OPTIONAL),
                        'autoenrol' => new external_value(PARAM_BOOL, 'Permite al servicio automatricular usuarios en el curso en el que se esta solicitando el acceso. Los valores posibles para este parámetro son "1" para soportar automatriculación en el curso o "0" para no dar soporte a la automatriculación. En este último caso si el usuario no estuviera matriculado el servicio devolverá un código de error indicando tal circuntancia y  no validará al usuario en el sistema. Por defecto toma el valor "1"', VALUE_DEFAULT, true)
                )
        );
    }

    /**
     * Returns welcome message
     * @return string welcome message
     */
    public static function get_email_login($email=null, $password=null, $courseid=1, $autoenrol=true) {
         global $DB, $CFG;

        //Parameter validation
        //REQUIRED
        $params = self::validate_parameters(self::get_email_login_parameters(),
                array('email' => $email, 'password' => $password, 'courseid' => $courseid, 'autoenrol' => $autoenrol));
        
        if ($user = $DB->get_record('user', array('email'=>$email))) {
            $code =  ( validate_internal_user_password($user, $password) ) ? WSLOGIN_NO_LOGIN_ERRROR : WSLOGIN_WRONG_PASSWORD;
            
            if ($course = $DB->get_record('course', array('id'=>$courseid))) {
                if ( !$autoenrol && !can_access_course($course, $user,'',true) ){
                    $code =  WSLOGIN_NO_ACCESS_TO_COURSE;
                }
            } else {
                $code = WSLOGIN_COURSE_NO_EXISTS;
            }
        
        } else {
            $code = WSLOGIN_USER_NO_EXISTS;
        }


        
    
      
        switch ($code) {
            case WSLOGIN_NO_LOGIN_ERRROR:
            case WSLOGIN_WRONG_PASSWORD:
                
                $time = time();
                $token = md5( "{$user->username}_{$course->shortname}_{$time}" ) ;
                
                $accessobj = new stdClass();
                $accessobj->token=$token;
                $accessobj->userid = $user->id;
                $accessobj->courseid = $course->id;
                $accessobj->authenticated = ($code == WSLOGIN_NO_LOGIN_ERRROR) ? 1 : 0;
                $accessobj->date = $time;
                $accessobj->autoenrol = $autoenrol;
                
                if ( $DB->insert_record('local_wslogin_access', $accessobj, false) ) {
                    $url = "{$CFG->wwwroot}/local/wslogin/route.php/{$token}";
                    $info = get_string('tokenOK', 'local_wslogin');
                } else {
                    $code = WSLOGIN_TOKEN_CREATION_ERROR;
                    $info = get_string('tokenerrorcreation', 'local_wslogin');
                    $url = '';
                }
                break;
         
            case WSLOGIN_USER_NO_EXISTS:
                $info = get_string('invaliduser', 'local_wslogin');
                $url = '';
                break;
                   
            case WSLOGIN_NO_ACCESS_TO_COURSE:
                $info = get_string('noaccesscourse', 'local_wslogin');
                $url = '';
                break;
                
            case WSLOGIN_COURSE_NO_EXISTS:
                $info = get_string('invalidcourse', 'local_wslogin');
                $url = '';
                break;
        }
        
        $loginresult = array(
                                'result'=> array(
                                    'code'=>$code,
                                    'info'=>$info
                                ),
                                'data'=>$url          
                            );
        return $loginresult;
    }

    /**
     * Returns description of method  get_email_login result value
     * @return external_description
     */
    public static function get_email_login_returns() {
        return new external_single_structure(
                    array (
                    'result' => new external_single_structure(
                        array(          
                            'code' => new external_value(PARAM_INT, 'Código generado en el proceso de validación. Los valores de código error soportados son: 0 - Sin error; 1 - La contraseña proporcionada no es correcta; 2 - El email proporcionado no existe en el sistema; 3 - El usuario no está matriculado en el curso (sólo si el modo de automatricula está inactivo)'),
                            'info' => new external_value(PARAM_TEXT, 'Resultado obtenido de la consulta al servicio. En caso de éxito (code=0) devuelve una url que da acceso al usuario al curso con el token de validación correspondiente. En caso de fracaso devuelve la descripción correspondiente del mensje de error')
                        )
                    ),
                    'data' => new external_value(PARAM_RAW, 'Resultado obtenido de la consulta al servicio. En caso de éxito (code=0) devuelve una url que da acceso al usuario al curso con el token de validación correspondiente. En caso de fracaso devuelve la descripción correspondiente del mensje de error') 
            )
        );
    }



}


class local_wslogin_exception extends moodle_exception {
        /**
         * Constructor
         */
        function __construct($errorcode, $debuginfo='') {
            parent::__construct($errorcode, 'local_wslogin', '', null, $debuginfo);
        }
    }