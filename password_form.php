<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir.'/formslib.php');

class wslogin_password_form extends moodleform {

    function definition() {
        $mform =& $this->_form;
        
        $customdata =& $this->_customdata;
        
        $mform->addElement('header', 'general', get_string('servicepasswordincorrect', 'local_wslogin'));

        $mform->addElement('static', 'email', get_string('email'), $customdata['email']);
        
        $mform->addElement('password', 'password', get_string('password'));
        $mform->setType('password', PARAM_RAW);
        $mform->addRule('password', get_string('nopassword'), 'required', null, 'client');


        $this->add_action_buttons(false, get_string('validate', 'local_wslogin'));

        $mform->addElement('hidden', 'courseid', $customdata['courseid']);
        $mform->setType('courseid', PARAM_INT);

        $mform->addElement('hidden', 'userid', $customdata['userid']);
        $mform->setType('userid', PARAM_INT);

    }
}
