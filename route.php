<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This script route external login request from a token.
 *
 * @package   localwslogin
 * @copyright 2012 Cadmo Conocimiento (http://www.cadmoweb.com/)
 * @author    Oscar Ruesga
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 
define('MAX_TOKEN_TTL', 60); //Tiempo de vida del token de validación en segundos
 
require_once('../../config.php');

$userid = optional_param('userid', 0, PARAM_INT);    // userid
$courseid = optional_param('courseid', 0, PARAM_INT);    // courseid
$password = optional_param('password', null, PARAM_TEXT);    // courseid

$time = time();
$acumulate_ttl = $time+MAX_TOKEN_TTL;
$DB->delete_records_select('local_wslogin_access', "date>'{$acumulate_ttl}'" );

//Sección de gestión de password incorrectos
if ( !empty($userid) && !empty($courseid) && !empty($password) ) {
    
    
    
    try {
        $user = $DB->get_record('user', array('id'=>intval($userid)), '*', MUST_EXIST);
    } catch (moodle_exception $e) {
        $message = ($e->getCode() == '"invalidrecord"') ? get_string('invaliduser', 'local_wslogin') : $e->getMessage();
        redirect($CFG->wwwroot, $message  . get_string('redirectsystemmessage', 'local_wslogin'), 2);
    }
    
    try {
        $course = $DB->get_record('course', array('id'=>intval($courseid)), 'id', MUST_EXIST);
    } catch (moodle_exception $e) {
        $message = ($e->getCode() == '"invalidrecord"') ? get_string('invalidcourse', 'local_wslogin') : $e->getMessage();
        redirect($CFG->wwwroot, $message  . get_string('redirectsystemmessage', 'local_wslogin'), 2);
    }
        
    
    if  ( validate_internal_user_password($user, $password) ) { 
        complete_user_login($user);
        $url = "{$CFG->wwwroot}/course/view.php?id={$course->id}";
        redirect($url);
    } else {
        $message = get_string('invalidpassword', 'local_wslogin');
        redirect($CFG->wwwroot, $message . get_string('redirectsystemmessage', 'local_wslogin'), 2);
    }
    
}



$token  = ltrim(get_file_argument(), '/');

//Obtengo el registro de acceso para el token proporcionado
try {
    $accessobj = $DB->get_record('local_wslogin_access', array('token'=>$token), '*', MUST_EXIST);
} catch (moodle_exception $e) {
    $message = ($e->getCode() == '"invalidrecord"') ? get_string('invalidtoken', 'local_wslogin') : $e->getMessage();
    redirect($CFG->wwwroot, $message, 2);
}


//Elimino el registro tratado
$DB->delete_records('local_wslogin_access', array("id"=>$accessobj->id) );


//Compruebo si el tiempo de vida del objeto de acceso a expirado
$token_ttl = $time - $accessobj->date;
if ($token_ttl > MAX_TOKEN_TTL) {
    $message = get_string('maxttlexceded', 'local_wslogin');
    redirect($CFG->wwwroot, $message, 2);
}



//obtengo el objeto de usuario a partir del registro de acceso
try {
    $user = $DB->get_record('user', array('id'=>intval($accessobj->userid)), '*', MUST_EXIST);
} catch (moodle_exception $e) {
    $message = ($e->getCode() == '"invalidrecord"') ? get_string('invaliduser', 'local_wslogin') : $e->getMessage();
    redirect($CFG->wwwroot, $message, 2);
}

//Obtengo el objeto curso a partir del registro de acceso
try {
    $course = $DB->get_record('course', array('id'=>intval($accessobj->courseid)), 'id', MUST_EXIST);
} catch (moodle_exception $e) {
    $message = ($e->getCode() == '"invalidrecord"') ? get_string('invalidcourse', 'local_wslogin') : $e->getMessage();
    redirect($CFG->wwwroot, $message, 2);
}


if ($accessobj->authenticated !== "1")  {
    $msg_setpassword = get_string ('setpassword', 'local_wslogin');
    
    $PAGE->set_url('/local/wslogin/route.php');
    $PAGE->set_context(context_system::instance());
    $PAGE->set_pagelayout('login');
    $PAGE->set_title($msg_setpassword);
    
    echo $OUTPUT->header();
    echo $OUTPUT->heading($msg_setpassword, 4);
    
   
    require_once('./password_form.php');
    
    $data = array();
    $data['userid'] = $user->id;
    $data['email'] = $user->email;
    $data['courseid'] = $course->id;

    $password_form = new wslogin_password_form(null, $data);
    $password_form->display();
    

    
    echo $OUTPUT->footer();
    die;
}

//Compruebo si el usuario tiene permisos de acceso sobre el curso o es un administrador del mismo

if ( !can_access_course($course, $user,'',true) ){
    if ($accessobj->autoenrol === "1") {
       $instance = $DB->get_record('enrol', array('courseid'=>$course->id, 'enrol'=>'manual'), '*', MUST_EXIST);
        $plugin = enrol_get_plugin('manual');
        if ($plugin->allow_enrol($instance) ) {
            $plugin->enrol_user($instance, $user->id, 5);
        } else {
            throw new enrol_ajax_exception('enrolnotpermitted');
        }
       
    } else {
        $message = get_string('noaccesscourse', 'local_wslogin');
        redirect($CFG->wwwroot, $message, 2);  
    }
     
}


complete_user_login($user);
$url = "{$CFG->wwwroot}/course/view.php?id={$course->id}";
redirect($url);