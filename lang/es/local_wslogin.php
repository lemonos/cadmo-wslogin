<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Web service template plugin related strings
 * @package   localwslogin
 * @copyright 2012 Cadmo Conocimiento (http://www.cadmoweb.com/)
 * @author    Jerome Mouneyrac
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Web Service Login Externo';

$string['invalidtoken'] = 'Código de acceso incorrecto.';
$string['maxttlexceded'] = 'El tiempo máximo de vida del acceso ha expirado.';
$string['invaliduser'] = 'El usuario solicitado no existe en el sistema.';
$string['invalidcourse'] = 'El curso solicitado no existe en el sistema.';
$string['noaccesscourse'] = 'El usuario no tiene permisos para acceder al curso.';
$string['tokenerrorcreation'] = 'No se ha podido crear correctamente el token de autenticación.';
$string['invalidpassword'] = 'La contraseña introducida no es correcta.';
$string['redirectsystemmessage'] = ' A continuación el sistema te redirigirá a la página identificación por defecto.';
$string['setpassword'] = 'Introduce la contraseña';
$string['servicepasswordincorrect'] = 'La contraseña proporcionada no es correcta. Por favor, vuelva a introducir la contraseña';
$string['validate'] = 'Validar';

$string['tokenOK'] = 'El token se ha creado correctamente';
