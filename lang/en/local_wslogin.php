<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Web service template plugin related strings
 * @package   localwslogin
 * @copyright 2012 Cadmo Conocimiento (http://www.cadmoweb.com/)
 * @author    Oscar Ruesga
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Web Services External Login';

$string['invalidtoken'] = 'Incorrect access code.';
$string['maxttlexceded'] = 'The maximum lifetime access has expired.';
$string['invaliduser'] = 'The request user doesn\'t exists in the system.';
$string['invalidcourse'] = 'The request course doesn\'t exists in the system.';
$string['noaccesscourse'] = 'The user does not have permission to access the course.';
$string['tokenerrorcreation'] = 'Unable to successfully create authentication token.';
$string['invalidpassword'] = 'The password is not entered correctly.';
$string['redirectsystemmessage'] = ' Then you\'ll automatically be redirected to the default login page.';
$string['setpassword'] = 'Introduce the password';
$string['servicepasswordincorrect'] = 'The password provided is incorrect. Please re-enter the password';
$string['validate'] = 'Validate';

$string['tokenOK'] = 'The token is created successfully';