<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Web service local plugin login external functions and service definitions.
 *
 * @package    localwslogin
 * @copyright  2012 Oscar Ruesga
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// We defined the web service functions to install.
$functions = array(
        'local_wslogin_get_email_login' => array(
                'classname'   => 'local_wslogin_external',
                'methodname'  => 'get_email_login',
                'classpath'   => 'local/wslogin/externallib.php',
                'description' => 'Autentica a un usuario desde un sistema externo a través de una dirección de correo electrónico y contraseña valida en el repositorio de usuarios de moodle. Retorna la URL que da soporte al acceso del usuario al courso específico. Adicionalmente el servicio puede matricular al usuario en un curso específico.',
                'type'        => 'write',
        )
);

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
$services = array(
        'External Login' => array(
                'functions' => array (
                                        'local_wslogin_get_email_login',
                                        'core_user_create_users'),
                'restrictedusers' => 1,
                'shortname' => 'wslogin',
                'enabled'=>0
        )
);
